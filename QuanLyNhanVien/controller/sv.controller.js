function layThongTinTuForm() {
  const taiKhoan = document.getElementById("tknv").value;
  const hoVaTen = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const matKhau = document.getElementById("password").value;
  const ngayLam = document.getElementById("datepicker").value;
  const luongCB = document.getElementById("luongCB").value;
  const chucVu = document.getElementById("chucvu").value;
  const gioLam = document.getElementById("gioLam").value;

  return new NhanVien(
    taiKhoan,
    hoVaTen,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
}

function renderDSNV(nvArr) {
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    console.log(nv);
    var trContent = `<tr>
      <td>${nv.taiKhoan}</td>
      <td>${nv.ten}</td>
      <td>${nv.email}</td>
      <td>${nv.ngayLam}</td>
      <td>${nv.chucVu}</td>
      <td>${nv.tongLuong()}</td>
      <td>${nv.xepLoai()}</td>
      <td>
    <button onclick="xoaNhanVien('${
      nv.taiKhoan
    }')" class="btn btn-danger ">Xóa</button>

    <button onclick="suaNhanVien('${nv.taiKhoan}')" data-toggle="modal"
    data-target="#myModal" class="btn btn-warning">Sửa</button>
    </td>
      </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
