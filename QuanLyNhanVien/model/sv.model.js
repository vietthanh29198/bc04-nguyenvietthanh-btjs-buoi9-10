function NhanVien(
  taiKhoan,
  ten,
  email,
  matKhau,
  ngayLam,
  luongCB,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCB = luongCB;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCB * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongCB * 2;
    } else {
      return this.luongCB * 1;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      return "nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      return "nhân viên khá";
    } else {
      return "nhân viên trung bình";
    }
  };
}
