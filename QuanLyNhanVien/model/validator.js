var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    }
  },

  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    }
  },

  kiemTraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (re.test(value)) {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    }
  },

  kiemTraLaChu: function (value, idError, message) {
    const re = /[0-9]/i;

    for (let char of value) {
      if (re.test(char)) {
        document.getElementById(idError).innerHTML = message;
        document
          .getElementById(idError)
          .classList.replace("sp-thongbao", "sp-show-thongbao");
        return false;
      } else {
        document.getElementById(idError).innerHTML = "";
        document
          .getElementById(idError)
          .classList.replace("sp-show-thongbao", "sp-thongbao");
      }
    }
    return true;
  },

  kiemTraMatKhau: function (value, idError, message) {
    const re =
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{3,}$/i;
    if (re.test(value)) {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    }
  },

  kiemTraDinhDangNgay: function (value, idError, message) {
    var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/i;

    if (date_regex.test(value)) {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    }
  },

  kiemTraGioiHanSo: function (value, idError, message, min, max) {
    if (value < min || value > max) {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    }
  },

  kiemTraChucVu: function (value, idError, message) {
    if (value == "Chọn chức vụ") {
      document.getElementById(idError).innerHTML = message;
      document
        .getElementById(idError)
        .classList.replace("sp-thongbao", "sp-show-thongbao");
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      document
        .getElementById(idError)
        .classList.replace("sp-show-thongbao", "sp-thongbao");
      return true;
    }
  },
};
