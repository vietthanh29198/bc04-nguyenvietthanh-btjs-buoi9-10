const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";
var dsnv = [];

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);
  for (var index = 0; index < dsnv.length; index++) {
    nv = dsnv[index];

    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matKhau,
      nv.ngayLam,
      nv.luongCB,
      nv.chucVu,
      nv.gioLam
    );
  }

  renderDSNV(dsnv);
}

function btnThemNhanVien() {
  hideMessageError();
  document
    .getElementById("btnCapNhat")
    .classList.replace("showBtnUpdate", "hideBtnUpdate");
  document
    .getElementById("btnThemNV")
    .classList.replace("hideBtnAdd", "showBtnAdd");
  document.getElementById("header-title").innerText = "Add";
  document.getElementById("tknv").value = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("luongCB").value = "";
  document.getElementById("gioLam").value = "";
}

function formValidation(nv) {
  // kiem tra tai khoan
  var isValid =
    validator.kiemTraRong(
      nv.taiKhoan,
      "tbTKNV",
      "Tài khoản không được để trống."
    ) &&
    validator.kiemTraDoDai(
      nv.taiKhoan,
      "tbTKNV",
      "Tài khoản từ 4 tới 6 kí tự.",
      4,
      6
    );

  // kiem tra ho va ten
  isValid =
    isValid &
    (validator.kiemTraRong(nv.ten, "tbTen", "Họ và tên không được để trống.") &&
      validator.kiemTraLaChu(nv.ten, "tbTen", "Họ và tên phải là chữ."));

  // kiem tra email
  isValid =
    isValid &
    (validator.kiemTraRong(nv.email, "tbEmail", "Email không được để trống.") &&
      validator.kiemTraEmail(
        nv.email,
        "tbEmail",
        "Email phải là user@gmail.com."
      ));

  //kiem tra mat khau
  isValid =
    isValid &
    (validator.kiemTraRong(
      nv.matKhau,
      "tbMatKhau",
      "Mật khẩu không được để trống."
    ) &&
      validator.kiemTraDoDai(
        nv.matKhau,
        "tbMatKhau",
        "Mật khẩu phải từ 6 tới 10 kí tự.",
        6,
        10
      ) &&
      validator.kiemTraMatKhau(
        nv.matKhau,
        "tbMatKhau",
        "Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa , 1 ký tự đặc biệt."
      ));

  // kiem tra ngay lam
  isValid =
    isValid &
    (validator.kiemTraRong(
      nv.ngayLam,
      "tbNgay",
      "Ngày làm không được để trống."
    ) &&
      validator.kiemTraDinhDangNgay(
        nv.ngayLam,
        "tbNgay",
        "Ngày làm phải là mm/dd/yyyy."
      ));

  // kiem tra luong co ban
  isValid =
    isValid &
    (validator.kiemTraRong(
      nv.luongCB,
      "tbLuongCB",
      "Lương cơ bản không được để trống."
    ) &&
      validator.kiemTraGioiHanSo(
        nv.luongCB,
        "tbLuongCB",
        "Lương cơ bản từ 1000000 tới 20000000.",
        1000000,
        20000000
      ));

  // kiem tra chuc vu
  isValid =
    isValid &
    validator.kiemTraChucVu(nv.chucVu, "tbChucVu", "Bắt buộc chọn chức vụ.");

  // kiem tra gio lam
  isValid =
    isValid &
    (validator.kiemTraRong(
      nv.gioLam,
      "tbGiolam",
      "Giờ làm không được để trống."
    ) &&
      validator.kiemTraGioiHanSo(
        nv.gioLam,
        "tbGiolam",
        "Giờ làm trong tháng phải từ 80-200 giờ ",
        80,
        200
      ));
  return isValid;
}

function themNhanVien() {
  var newNv = layThongTinTuForm();

  var isValid = formValidation(newNv);

  if (isValid == false) {
    return;
  } else {
    dsnv.push(newNv);
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
    document.getElementById("tknv").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    document.getElementById("luongCB").value = "";
    document.getElementById("gioLam").value = "";
  }
}

function xoaNhanVien(id) {
  console.log(id);
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
  console.log(index);
  dsnv.splice(index, 1);
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
  renderDSNV(dsnv);
}

function suaNhanVien(id) {
  hideMessageError();
  document
    .getElementById("btnThemNV")
    .classList.replace("showBtnAdd", "hideBtnAdd");
  document
    .getElementById("btnCapNhat")
    .classList.replace("hideBtnUpdate", "showBtnUpdate");
  document.getElementById("header-title").innerText = "Update";

  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
  console.log("index", index);
  if (index != -1) {
    var nv = dsnv[index];
    showThongTinLenForm(nv);
  }
}

function hideMessageError() {
  var messageElementIds = [
    "tbTKNV",
    "tbTen",
    "tbEmail",
    "tbMatKhau",
    "tbNgay",
    "tbLuongCB",
    "tbChucVu",
    "tbGiolam",
  ];

  messageElementIds.forEach((messageElementId) => {
    document
      .getElementById(messageElementId)
      .classList.replace("sp-show-thongbao", "sp-thongbao");
  });
}

function capNhatNhanVien() {
  var taikhoan = document.getElementById("tknv").value;
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == taikhoan;
  });
  var newNv = layThongTinTuForm();

  var isValid = formValidation(newNv);
  if (!isValid) {
    return;
  } else {
    dsnv[index] = newNv;
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
  }
}

function timKiemNhanVien() {
  var searchKey = document.getElementById("searchName").value;

  var dsnvFiltered = dsnv.filter(function (nv) {
    return nv.xepLoai().includes(searchKey);
  });
  renderDSNV(dsnvFiltered);
}
